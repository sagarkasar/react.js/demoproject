import { Paper, Container, Stack, Button, TextField, Typography, ButtonGroup, FormGroup, FormControlLabel, Switch } from '@mui/material';
import React, {useState} from 'react'
import {  useNavigate,  } from 'react-router-dom';
import About from './About';
import Contact from './Contact';
import Footer from './Footer';
import Header from './Header';
import Sample from './Sample';

function Home(props) {
    let navigate = useNavigate();
    const myElement = <Car brand="Ford" />;
    const Name = 'Anvay';
    const address = 'Nashik';
    const [Count, setCount] = useState(0);
    const [myName, setMyname] = useState('Mango');
    const [Style, setStyle] = useState(false);
    const [Color, setColor] = useState('');
    const handleClickAdd = () => {
        setCount(Count + 1)
    }

    const handleClickSub = () => {
        if(Count>=0){
            setCount(Count - 1)
        } 
    }
    const [myCar, setMyCar] = useState("Volvo");

    const handleChange = (event) => {
      setMyCar(event.target.value)
    }
    function Car(props) {
        return <h2>My car Name is { props.brand }!</h2>;
    }
    const add = (a, b) => {
        let additionis = a + b;
        return <p>{additionis}</p>
    }
    const colorGray = () => {
        setStyle(!Style);
        setColor('selectedGray');
    }
    const colorYellow = () => {
        setStyle(!Style);
        setColor('selectedPink');
    }
    return (
        <div>
            
            <Container className='containerOther' maxWidth="xl">
            <Header/>
                <h1>Welcome to Supra infotech</h1>
                <h2>Hello Everyone</h2>
                <h3>This is React.js Training session </h3>
                <h4>my Name is {Name} i am from {address} and my age is {Count}</h4>
                <h5>welcome to JavaScript</h5>
                <ol style={{ backgroundColor:'lightblue'}}>
                    <li style={{fontSize: 25}}>Sagar</li>
                    <li style={{color: 'blue'}}>Gautami</li>
                    <li style={{fontWeight: '700'}}>Vaishali</li>
                    <li style={{textDecoration:'underline'}}>Atharva</li>
                </ol>
                <ul style={{backgroundColor:'lightpink'}}>
                    <li style={{textDecoration:'underline', textDecorationColor: 'red', textDecorationStyle: 'dashed'}}>Abhay</li>
                    <li><u>Kunal</u></li>
                    <li><i>Nilesh</i></li>
                    <li><b>Kailas</b></li>
                </ul>
                {add(5, 5)}
                <Sample fname='Sagar' lname='Kasar' city='Nashik' age='27' education='BE comp'/>
                <Car brand="Ford" />
                <Car brand="Volvo" />
                <Car brand="Fiat" />
                {myElement}
                <form>
                <select value={myCar} onChange={handleChange}>
                    <option value="Ford">Ford</option>
                    <option value="Volvo">Volvo</option>
                    <option value="Fiat">Fiat</option>
                </select>
                </form>
                <FormGroup>
                <FormControlLabel control={<Switch defaultChecked />} label="Label" />
                <FormControlLabel disabled control={<Switch />} label="Disabled" />
                </FormGroup>
                <Typography className='txt'>Sagark <br />{2 + 2}</Typography>
                <Typography className='txt'> Count is: {Count}</Typography>
                <Typography className={`fruitsTxt Style? ${setColor}:${setColor}`}>My favourite fruit is {myName}</Typography>
                <Stack direction='row' spacing={2}>
                    <Button className='txt' variant='contained' onClick={() => setMyname('Orange')}>Fruits</Button>
                    <Button className='txt' variant='contained' onClick={()=>setMyname('Mango')}>Fruits</Button>
                </Stack>
                <Stack direction='row' spacing={2}>
                    <Button className='txt' variant='contained' onClick={colorGray}>Background Color</Button>
                    <Button className='txt' variant='contained' onClick={colorYellow}>Background Color</Button>
                </Stack>
                <Stack direction='row' spacing={2}>
                    <Button onClick={handleClickAdd} className='btn'>Addition</Button>
                    <Button onClick={handleClickSub} className='btn'>Substraction</Button>
                </Stack>
                <Footer />
            </Container>
        </div>
  )
}

export default Home