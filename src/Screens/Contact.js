import { Container, Typography } from '@mui/material'
import React from 'react'
import Footer from './Footer'
import Header from './Header'

function Contact() {
  return (
    <div>
      <Container className='containerOther' maxWidth="xl">
        <Header />
        <Typography>Contact Page</Typography>
      </Container>
      <Footer/>
    </div>
  )
}

export default Contact