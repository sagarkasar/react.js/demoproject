import { Paper, Container, Stack, Button, TextField } from '@mui/material';
import React from 'react'
import {  useNavigate,  } from 'react-router-dom';
import Footer from './Footer';
function Signup() {
    let navigate = useNavigate();
  return (
    <div>
        {/* <Stack className='headerView' spacing={1.2}>
        <Button variant='contained' className='btnHeader' onClick={()=>navigate('/')}>Home</Button>
        <Button variant='contained' className='btnHeader' onClick={()=>navigate('/about')}>About</Button>
        <Button variant='contained' className='btnHeader' onClick={()=>navigate('/contact')}>Contact</Button>
        </Stack> */}
        <Container className='container' maxWidth="xl">
                <Paper elevation={3} className="paper" spacing={2} >
                  <h2 className='headertxt'>Sign up</h2>
                    <TextField label="First name" variant="outlined" className='inputfield' />
                    <TextField label="Last name" variant="outlined" className='inputfield'/>
                    <TextField label="Email-id" variant="outlined" className='inputfield'/>
                    <TextField label="Password" variant="outlined" className='inputfield'/>
                  <Button variant='contained' color='primary' className='btn' onClick={() => navigate('/')}>Submit</Button>
                  <p className='txtBottom'>already have an account <Button onClick={()=>navigate('/')}>signin here!</Button></p>
        </Paper>
      </Container>
      <Footer/>
        </div>
  )
}

export default Signup