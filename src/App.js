
import './App.css';
import '../src/Stylesheet/CommonStyle.css';
import { Routes, Route, BrowserRouter  } from "react-router-dom";
import Home from './Screens/Home';
import About from './Screens/About';
import Contact from './Screens/Contact';
import Signin from './Screens/Signin';
import Signup from './Screens/Signup';
import Forgotpass from './Screens/Forgotpass';
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Signin />} />
        <Route path="signup" element={<Signup />} />
        <Route path="forgotpass" element={<Forgotpass />} />
        <Route path="home" element={<Home />} />
        <Route path="about" element={<About />} />
        <Route path="contact" element={<Contact />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
