import { Container, Typography } from '@mui/material'
import React from 'react'
import Footer from './Footer'
import Header from './Header'

function About(prop) {
  return (
    <div>
      <Container className='containerOther' maxWidth="xl">
        <Header />
        <Typography>About Page</Typography>
      </Container>
      <Footer/>
    </div>
  )
}

export default About