import { Button, Stack } from '@mui/material'
import React from 'react'
import { useNavigate } from 'react-router-dom';

function Header() {
    let navigate = useNavigate();
    return (
    <div>
        <Stack direction='row' spacing={2} className='headerView'> 
            <Button className='headerBtn' variant='outlined' onClick={() => navigate('/Home') }>Home</Button>
            <Button className='headerBtn' variant='outlined' onClick={() => navigate('/about') }>About</Button>
            <Button className='headerBtn' variant='outlined' onClick={() => navigate('/contact') }>Contact</Button>
        </Stack>
    </div>
  )
}

export default Header