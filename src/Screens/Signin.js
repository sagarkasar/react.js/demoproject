import { Password } from '@mui/icons-material';
import { Paper, Container, Stack, Button, TextField, Typography } from '@mui/material';
import React,{useState} from 'react'
import {  Link, useNavigate,  } from 'react-router-dom';
import Contact from './Contact';
import Footer from './Footer';
import * as yup from 'yup';
import { Formik } from 'formik';

function Signin() {
  let navigate = useNavigate();
  const [Email, setEmail] = useState('');
  const [Password, setPassword] = useState('')

  const onSubmit = (Values) => {
    console.log("Values", Values)
    alert("email " + Values.Email + "password "+ Values.Password)
    //alert(`The name you entered was: ${name}`)
    navigate('home');
    console.log('Email: '+ Email + " password: " + Password)
  }
  return (
    <div>
        {/* <Stack className='headerView' spacing={1.2}>
        <Button variant='contained' className='btnHeader' onClick={()=>navigate('/')}>Home</Button>
        <Button variant='contained' className='btnHeader' onClick={()=>navigate('/about')}>About</Button>
        <Button variant='contained' className='btnHeader' onClick={()=>navigate('/contact')}>Contact</Button>
        </Stack> */}
        <Container className='container' maxWidth="xl">
                <Paper elevation={3} className="paper" spacing={2} >
          <h2 className='headertxt'>Sign in</h2>
          {/* <p>"Email"  {Email} "password"  {Password}</p> */}
          {/* <form onSubmit={handleSubmit}> */}
          <Formik
          initialValues={{
            Email: "",
            Password: "",
          }}
          onSubmit={onSubmit}
          validationSchema={yup.object().shape({
            Email: yup.string().email().required(),
            Password: yup.string().required(),
          })}>
          {({
            values,
            handleChange,
            errors,
            setFieldTouched,
            touched,
            isValid,
            handleSubmit
          }) => (
          <div>
            <TextField
              fullWidth
            label="Email-id"
            className='inputfield'
            color='success' 
            defaultValue={values.Email}
                // onChangeText={handleChange('email')}
                // onChange={(e) => setEmail(e.target.value)}
                //   onBlur={() => setFieldTouched('email')}
                  onChange={handleChange("Email")}
                  onBlur={() => setFieldTouched("Email")}
                  onChangeCapture={(e) => setEmail(e.target.value)}
                />
                {touched.Email && errors.Email && (
                <Typography className='error'>{errors.Email}</Typography>
              )}
            <TextField
              fullWidth
            label="Password"
            variant="outlined"
            className='inputfield' 
            defaultValue={values.Password}
                // onChangeText={handleChange('password')}
                // onChange={(e) => setPassword(e.target.value)}
                //   onBlur={() => setFieldTouched('password')}
                  onChange={handleChange("Password")}
                  onBlur={() => setFieldTouched("Password")}
                  onChangeCapture={(e) => setPassword(e.target.value)}
                />
                {touched.Password && errors.Password && (
                <Typography className='error'>{errors.Password}</Typography>
              )}
            <Button type='submit' onClick={handleSubmit} disabled={!isValid} variant='contained' color='primary' className='btn'>Submit</Button>
            </div>  )}
                </Formik>
         {/*  </form> */}
                  <p className='txtBottom'>Don't have an account <Button onClick={()=>navigate('/signup')}>signup here!</Button></p>
        </Paper>
      </Container>
      <Footer/>
        </div>
  )
}

export default Signin