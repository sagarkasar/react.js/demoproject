import React from 'react'

function Sample(props) {
  return (
      <div>
          Props Practice 
          <p>
            My first name is {props.fname}<br />
            my last name is {props.lname}<br />
            my city is {props.city}<br />
            my age is {props.age}<br />
            my education is {props.education}
          </p>
      </div>
  )
}

export default Sample