import { Stack, Typography } from '@mui/material'
import React from 'react'

function Footer() {
  return (
      <div>
        <Stack direction='row' spacing={2} className='FooterView'>
              <Typography>@2022 Copywrite reserved Supra infotech</Typography>
              <Typography>Designer SagarK</Typography>
        </Stack>
      </div>
  )
}

export default Footer